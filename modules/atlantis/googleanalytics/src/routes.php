<?php
/*
 * Routes: GoogleAnalytics
 * @Atlantis CMS
 * v 1.0
 */

Route::get('admin/modules/googleanalytics', 'Module\GoogleAnalytics\Controllers\Admin\GoogleAnalyticsAdminController@getIndex');
Route::post('admin/modules/googleanalytics/update', 'Module\GoogleAnalytics\Controllers\Admin\GoogleAnalyticsAdminController@postUpdate');

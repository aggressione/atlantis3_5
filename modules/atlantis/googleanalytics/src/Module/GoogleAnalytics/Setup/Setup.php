<?php


/**
 * Setup: GoogleAnalytics
 * @Atlantis CMS
 * v 1.2
 */

return [
    'name' => 'Google Analytics',
    'author' => 'Atlantis CMS',
    'version' => '1.2',
    'adminURL' => 'admin/modules/googleanalytics', // admin/modules/googleanalytics
    /**
     * ex. icon icon-Files
     * http://docteur-abrar.com/wp-content/themes/thunder/admin/stroke-gap-icons/index.html
     *
     * ex. fa fa-beer
     * http://fontawesome.io/icons/
     */
    'icon' => 'icon icon-ChartUp',
    'path' => 'atlantis/googleanalytics/src',
    'moduleNamespace' => 'Module\GoogleAnalytics',
    'seedNamespace' => 'Module\GoogleAnalytics\Seed',
    'seeder' => '\Module\GoogleAnalytics\Seed\GoogleAnalyticsSeeder',
    'provider' => 'Module\GoogleAnalytics\Providers\GoogleAnalyticsServiceProvider',
    'migration' => 'modules/atlantis/googleanalytics/src/Module/GoogleAnalytics/Migrations/',
    'extra' => NULL,
    'description' => 'Embeds Google Analytics or Google Tag Manager tracking code',
        /**
        * 'create_pages' => [
        *   [
        *   'name' => 'Google Analytics',
        *   'url' => 'NULL',
        *   'body' => '<div data-pattern-func="module:Google Analytics@build"></div>'
        *   ]
        * ],
        * 'create_patterns' => [
        *   [
        *   'name' => 'Google Analytics',
        *   'url' => 'NULL',
        *   'language' => 'en',
        *   'status' => '1',
        *   'mask' => '',
        *   ]
        * ]
        */
   ];

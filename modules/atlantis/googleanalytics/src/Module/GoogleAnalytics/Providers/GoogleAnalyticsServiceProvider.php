<?php

namespace Module\GoogleAnalytics\Providers;

/*
 * Provider: GoogleAnalytics
 * @Atlantis CMS
 * v 1.0
 */

class GoogleAnalyticsServiceProvider extends \Illuminate\Support\ServiceProvider
{

  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [
      \Module\GoogleAnalytics\Commands\GoogleAnalyticsCommand::class
  ];

  public function register()
  {

    /** Register artisan commands * */
    //$this->commands($this->commands);

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Setup.php', "googleanalytics.setup"
    );

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Config.php', "googleanalytics.config"
    );

    $aConfig = \Config::get('googleanalytics.config');

    if (isset($aConfig['appBind'])) {
      foreach ($aConfig['appBind'] as $key => $value) {
        $this->app->bind($key, $value);
      }
    }

    $subscriber = new \Module\GoogleAnalytics\Events\GoogleAnalyticsEvent();

    \Event::subscribe($subscriber);

    //routes for modules should be included in the register method to preceed the base routes

    config(['modules-routes' => array_merge(config('modules-routes', []), [realpath(__DIR__ . '/../../../routes.php')])]);

    /**
     * register widgets
     */
    //\Atlantis\Widgets\Register::set(\Module\GoogleAnalytics\Widgets\GoogleAnalyticsWidget::class, \Config::get('googleanalytics.setup'));

  }

  public function boot()
  {

      /**
       * publishing assets to public folder
       * use "php artisan vendor:publish --tag=googleanalytics-assets --ansi --force" for manual publish
       */
      $this->publishes([__DIR__ . '/../Assets' => \Tools::getPublicPath(__DIR__ . '/../Assets')], 'googleanalytics-assets');

    //  load assests if any
    // \Atlantis\Helpers\Assets::registerScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', 10);

    /**
     * To register search and sitemap provider
     *
     * $t = \App::make('Transport');
     *
     *  $t->setEventValue("search.providers", [  'search' => 'Module\GoogleAnalytics\Models\Search' , 'weight' => 10] );
     *  $t->setEventValue("sitemap.providers", [  'sitemap' => 'Module\GoogleAnalytics\Models\Sitemap' , 'weight' => 10] );
     */


    $themeModViewPath = \Atlantis\Helpers\Themes\ThemeTools::getFullThemePath() . '/modules/googleanalytics/views/';

    if (is_dir($themeModViewPath)) {
      $this->loadViewsFrom($themeModViewPath, 'googleanalytics');
    } else {
      $this->loadViewsFrom(__DIR__ . '/../Views/', 'googleanalytics');
    }

    $this->loadViewsFrom(__DIR__ . '/../Views/', 'googleanalytics-admin');

    /**
    *  call this with trans('googleanalytics::file.key');
    */
    //$this->loadTranslationsFrom(__DIR__ . '/../Languages', "googleanalytics");

  }

}

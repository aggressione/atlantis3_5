<?php

namespace Module\Menus\Providers;

/*
 * Provider: Menus
 * @Atlantis CMS
 * v 1.0
 */

class MenusServiceProvider extends \Illuminate\Support\ServiceProvider
{

  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [
      \Module\Menus\Commands\MenusCommand::class
  ];

  public function register()
  {

    /** Register artisan commands * */
    //$this->commands($this->commands);

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Setup.php', "menus.setup"
    );

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Config.php', "menus.config"
    );

    $aConfig = \Config::get('menus.config');

    if (isset($aConfig['appBind'])) {
      foreach ($aConfig['appBind'] as $key => $value) {
        $this->app->bind($key, $value);
      }
    }

    $subscriber = new \Module\Menus\Events\MenusEvent();

    \Event::subscribe($subscriber);

    //routes for modules should be included in the register method to preceed the base routes

    config(['modules-routes' => array_merge(config('modules-routes', []), [realpath(__DIR__ . '/../../../routes.php')])]);

    /**
     * register widgets
     */
    //\Atlantis\Widgets\Register::set(\Module\Menus\Widgets\MenusWidget::class, \Config::get('menus.setup'));

  }

  public function boot()
  {

    /**
    * publishing assets to public folder
    * use "php artisan vendor:publish --tag=menus-assets --ansi --force" for manual publish
    */
    $this->publishes([__DIR__ . '/../Assets' => \Tools::getPublicPath(__DIR__ . '/../Assets')], 'menus-assets');

    //  load assests if any
    // \Atlantis\Helpers\Assets::registerScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', 10);

    /**
     * To register search and sitemap provider
     *
     * $t = \App::make('Transport');
     *
     *  $t->setEventValue("search.providers", [  'search' => 'Module\Menus\Models\Search' , 'weight' => 10] );
     *  $t->setEventValue("sitemap.providers", [  'sitemap' => 'Module\Menus\Models\Sitemap' , 'weight' => 10] );
     */


    $themeModViewPath = \Atlantis\Helpers\Themes\ThemeTools::getFullThemePath() . '/modules/menus/views/';

    if (is_dir($themeModViewPath)) {
      $this->loadViewsFrom($themeModViewPath, 'menus');
    } else {
      $this->loadViewsFrom(__DIR__ . '/../Views/', 'menus');
    }

    $this->loadViewsFrom(__DIR__ . '/../Views/', 'menus-admin');

    /**
    *  call this with trans('menus::file.key');
    */
    //$this->loadTranslationsFrom(__DIR__ . '/../Languages', "menus");

  }

}

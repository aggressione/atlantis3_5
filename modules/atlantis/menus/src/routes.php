<?php
/*
 * Routes: Menus
 * @Atlantis CMS
 * v 1.0
 */

Route::get('admin/modules/menus', [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'getIndex']);
Route::get('admin/modules/menus/add', [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'getAdd']);
Route::get('admin/modules/menus/edit/{id}', [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'getEdit']);
Route::post('admin/modules/menus/add', [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'postAdd']);
Route::post('admin/modules/menus/edit/{id}', [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'postEdit']);
Route::get('admin/modules/menus/delete/{id}' , [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'getDelete']);
Route::get('admin/modules/menus/bulk-action' , [\Module\Menus\Controllers\Admin\MenusAdminController::class, 'postBulkAction']);

<?php

namespace Module\CKEditor\Providers;

/*
 * Provider: CKEditor
 * @Atlantis CMS
 * v 1.0
 */

class CKEditorServiceProvider extends \Illuminate\Support\ServiceProvider
{

  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [
      \Module\CKEditor\Commands\CKEditorCommand::class
  ];

  public function register()
  {

    /** Register artisan commands * */
    //$this->commands($this->commands);

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Setup.php', "ckeditor.setup"
    );

    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Config.php', "ckeditor.config"
    );

    $aConfig = \Config::get('ckeditor.config');

    if (isset($aConfig['appBind'])) {
      foreach ($aConfig['appBind'] as $key => $value) {
        $this->app->bind($key, $value);
      }
    }

    //$subscriber = new \Module\CKEditor\Events\CKEditorEvent();

    //\Event::subscribe($subscriber);

    //routes for modules should be included in the register method to preceed the base routes

    config(['modules-routes' => array_merge(config('modules-routes', []), [realpath(__DIR__ . '/../../../routes.php')])]);

    /**
     * register widgets
     */
    //\Atlantis\Widgets\Register::set(\Module\CKEditor\Widgets\CKEditorWidget::class, \Config::get('ckeditor.setup'));

  }

  public function boot()
  {
      /**
       * publishing assets to public folder
       * use "php artisan vendor:publish --tag=ckeditor-assets --ansi --force" for manual publish
       */
      $this->publishes([__DIR__ . '/../Assets' => \Tools::getPublicPath(__DIR__ . '/../Assets')], 'ckeditor-assets');

    //  load assests if any
    // \Atlantis\Helpers\Assets::registerScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', 10);

    /**
     * To register search and sitemap provider
     *
     * $t = \App::make('Transport');
     *
     *  $t->setEventValue("search.providers", [  'search' => 'Module\CKEditor\Models\Search' , 'weight' => 10] );
     *  $t->setEventValue("sitemap.providers", [  'sitemap' => 'Module\CKEditor\Models\Sitemap' , 'weight' => 10] );
     */


    $themeModViewPath = \Atlantis\Helpers\Themes\ThemeTools::getFullThemePath() . '/modules/ckeditor/views/';

    if (is_dir($themeModViewPath)) {
      $this->loadViewsFrom($themeModViewPath, 'ckeditor');
    } else {
      $this->loadViewsFrom(__DIR__ . '/../Views/', 'ckeditor');
    }

    $this->loadViewsFrom(__DIR__ . '/../Views/', 'ckeditor-admin');

    /**
    *  call this with trans('ckeditor::file.key');
    */
    //$this->loadTranslationsFrom(__DIR__ . '/../Languages', "ckeditor");

  }

}

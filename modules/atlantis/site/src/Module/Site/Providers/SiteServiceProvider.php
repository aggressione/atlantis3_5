<?php

namespace Module\Site\Providers;

/*
 * Provider: Site
 * @Atlantis CMS
 * v 1.0
 */

class SiteServiceProvider extends \Illuminate\Support\ServiceProvider {

  public function register() {


    $this->mergeConfigFrom(
            __DIR__ . '/../Setup/Config.php', "site.config"
    );

    $aConfig = \Config::get('site.config');

    if (isset($aConfig['appBind'])) {
      foreach ($aConfig['appBind'] as $key => $value) {
        $this->app->bind($key, $value);
      }
    }

    //$subscriber = new \Module\Site\Events\SiteEvent();

    //\Event::subscribe($subscriber);

      /**
       * replace
       * include __DIR__ . '/../../../routes.php';
       * with
       */
      config(['modules-routes' => array_merge(config('modules-routes', []), [realpath(__DIR__ . '/../../../routes.php')])]);
  }

  public function boot() {

      /**
       * publishing assets to public folder
       * use "php artisan vendor:publish --tag=site-assets --ansi --force" for manual publish
       */
      $this->publishes([__DIR__ . '/../Assets' => \Tools::getPublicPath(__DIR__ . '/../Assets')], 'site-assets');


      $themeModViewPath = \Atlantis\Helpers\Themes\ThemeTools::getFullThemePath() . '/modules/site/views/';

    if (is_dir($themeModViewPath)) {
      $this->loadViewsFrom($themeModViewPath, 'site');
    } else {
      $this->loadViewsFrom(__DIR__ . '/../Views/', 'site');
    }

  }

}

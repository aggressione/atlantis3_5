<?php

return [
    'name' => 'Theme 101',
    'version' => 1,
    'author' => 'Atlantis team',
    'description' => 'Default Atlantis 3 Theme',
    'screenshot' => 'screenshot.png',
    'pattern_outputs' => [
        'patt_footer' => 'footer (output variable description)',
        'patt_header' => 'header (some description for variable)'
    ],
    'pattern_attributes' => [
        'footer' => [
            'preview_image_path' => '/assets/patterns-screenshots/slider.jpg',
            'attributes' => [
                [
                    'key' => 'images',
                    'default_value' => [],
                    'name' => 'Left box title',
                    'help_text' => 'Image description used for slider text. HTML allowed',
                    'columns' => '12',
                    'type' => 'images',
                ],
            ]
        ],
        'header' => [
            'preview_image_path' => '/assets/patterns-screenshots/mixedcontent.png',
            'attributes' => [
                [
                    'key' => 'title_1',
                    'default_value' => '',
                    'name' => 'Left box title',
                    'help_text' => '',
                    'columns' => '4',
                    'type' => 'text',
                ],
                [
                    'key' => 'link_url_1',
                    'default_value' => '',
                    'name' => 'Left box button url',
                    'help_text' => '',
                    'columns' => '4',
                    'type' => 'text',
                ],
                [
                    'key' => 'right_image',
                    'default_value' => '',
                    'name' => 'Right column image',
                    'help_text' => '',
                    'columns' => '12',
                    'type' => 'image',
                ],
            ]
        ]
    ]
];

